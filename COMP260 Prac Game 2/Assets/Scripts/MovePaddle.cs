﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]

public class MovePaddle : MonoBehaviour {
	
	//public float force = 50f;
	public float speed = 7f;
	
	private Rigidbody rigidbody;
	
	private bool isGameOver;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.useGravity = false;
		
		isGameOver = false;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if(isGameOver = false) {
			float yMove = Input.GetAxis("Horizontal");
			float xMove = Input.GetAxis("Vertical");
			
			
			Vector3 movement = new Vector3 (yMove, 0.0f, xMove);
			
			/*//Force Method
			rigidbody.AddForce ( movement * force);*/
			
			//Velocity Method
			rigidbody.velocity = new Vector3(yMove * speed, rigidbody.velocity.y, xMove * speed);
		}			
	}
	
	public void gameOver() {
		if(Scorekeeper.hasWon()){
			isGameOver = true;
		}
	}

	private Vector3 GetMousePosition() {
		//create a ray from the camera
		//passing through the mouse position
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		
		//find out where the ray intersects the XZ plane
		Plane plane = new Plane(Vector3.up, Vector3.zero);
		float distance = 0;
		plane.Raycast(ray, out distance);
		return ray.GetPoint(distance);
	}
	
	void OnDrawGizmos() {
		//draw the mouse ray
		Gizmos.color = Color.yellow;
		Vector3 pos = GetMousePosition();
		Gizmos.DrawLine(Camera.main.transform.position, pos);
	}
}
