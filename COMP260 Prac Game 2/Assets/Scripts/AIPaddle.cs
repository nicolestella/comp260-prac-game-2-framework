﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIPaddle : MonoBehaviour {
	
	public GameObject Puck;
	public GameObject puckStart;
	public GameObject startPos;
	public GameObject Goal;
	
	private Rigidbody rigidbody;
	private Vector3 targetPos;
	
	public float speed = 7f;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.useGravity = false;
	}

	void FixedUpdate() {
		
		Vector3 rightOfPuck = new Vector3(Goal.transform.position.x - Puck.transform.position.x, Puck.transform.position.y);
		
		if(Puck.transform.position.x > puckStart.transform.position.x) {
			//rigidbody.MovePosition(Vector3.MoveTowards(rigidbody.transform.position, rightOfPuck, 
									//speed * Time.fixedDeltaTime));
			targetPos = new Vector3(Puck.transform.position.x + 0.5f, Puck.transform.position.z);
		}
		else {
			targetPos = new Vector3(startPos.transform.position.x, Puck.transform.position.z);
		}
		
		rigidbody.MovePosition(Vector3.MoveTowards(rigidbody.transform.position, targetPos, speed * Time.fixedDeltaTime));
		
	}
}
