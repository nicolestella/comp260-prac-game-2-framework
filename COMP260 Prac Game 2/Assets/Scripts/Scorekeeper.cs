﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scorekeeper : MonoBehaviour {
	
	static private Scorekeeper instance;
	
	static public Scorekeeper Instance {
		get { return instance; }
	}
	
	public int pointsPerGoal = 1;
	private int[] score = new int[2];
	public Text[] scoreText;
	public int winAmount = 1;
	
	public GameObject gameOverPanel;
	public Text gameOverText;
	
	private bool gameOver = false;
	
	// Use this for initialization
	void Start () {		
		gameOverPanel.SetActive(false);
		
		if(instance == null) {
			//save this instance
			instance = this;
		}
		else {
			//more than one instance exists
			Debug.LogError("More than one Scorekeeper exists in the same scene.");
		}
		
		//reset the score to zero
		for(int i=0;i<score.Length;i++) {
			score[i] = 0;
			scoreText[i].text = "0";
		}
	}
	
	public void OnScoreGoal(int player) {
		score[player] += pointsPerGoal;
		
		if(score[player] == winAmount) {
			gameOver = true;
			gameOverPanel.SetActive(true);
			if(player == 1) {
				gameOverText.text = "Player Red Wins!";
			}
			else {
				gameOverText.text = "Player Blue Wins!";
			}
		}
		
		scoreText[player].text = score[player].ToString();
		Debug.Log(score[0]);
		Debug.Log(score[1]);
	}
	
	public static bool hasWon() {
		if(gameOver){
			return true;
		}
		else {
			return false;
		}
	}
}
